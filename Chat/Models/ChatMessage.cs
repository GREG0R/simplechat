﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chat.Models
{
    public class ChatMessage
    {
        // автор сообщения,  если Null - автор сервер
        public ChatUser User;
        // время сообщения
        public DateTime Date = DateTime.Now;
        // текст сообщения
        public string Text = "";
    }
}
