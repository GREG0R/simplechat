﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chat.Models
{
    public class ChatUser
    {
        public string Name;
        public DateTime LoginTime;
        public DateTime LastPing;
    }
}
